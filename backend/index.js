const express = require('express');
const mongoose = require('mongoose');
const app = express();
const User = require('./models/User');
const Message = require('./models/Message');
const cors = require('cors');
const config = require('./config');
const exitHook = require('async-exit-hook');
require('express-ws')(app);

const users = require('./app/users');
app.use(express.json());
app.use(cors());
app.use('/users', users);
const port = 8000;
const activeConnections = {};

const getActiveUsersAndMessages = async (type, messages) => {
    const activeUsers = [];

    for (let i = 0; i < Object.keys(activeConnections).length; i++) {
        const user = await User.findOne({token: Object.keys(activeConnections)[i]});
        activeUsers.push(user);
    }

    Object.keys(activeConnections).forEach(key => {
        const userConnection = activeConnections[key];
        userConnection.send(JSON.stringify({type: type, users: activeUsers, messages}));
    });
};

app.ws('/chat', async (ws, req) => {
    const token = req.query.token;
    const user = await User.findOne({token});

    function sendMessageToSpecificUser(user, data) {
        if (activeConnections[user] && activeConnections[user].readyState === ws.OPEN) {
            activeConnections[user].send(data);
        }
    }

    if (!user) {
        ws.send(JSON.stringify({type: 'ERROR', error: 'Unauthorized'}));
    } else {
        activeConnections[token] = ws;
        const allMessages = await Message.find().populate('userId', 'username');
        await getActiveUsersAndMessages('CONNECTED', allMessages.slice(-30));
    }

    ws.on('message', async msg => {
        const decoded = JSON.parse(msg);

        switch (decoded.type) {
            case 'CREATE_MESSAGE':
                try {
                    const message = new Message({message: decoded.message, userId: user._id});
                    await message.save();
                } catch {
                    ws.send(JSON.stringify({type: 'ERROR', error: 'Internal Server Error'}));
                }

                const allMessages = await Message.find().populate('userId', 'username');
                const lastMessage = allMessages[allMessages.length - 1];
                await getActiveUsersAndMessages('NEW_MESSAGE', lastMessage);
                break;

            case 'DELETE_MESSAGE':
                const message = await Message.findByIdAndDelete(decoded.messageId);
                await getActiveUsersAndMessages('DELETED_MESSAGE', message);
                break;

            case 'PRIVATE_MESSAGE':
                const specificUser = await User.findById(decoded.userId);
                const obj = {};

                obj[specificUser.token] = ws;

                try {
                    const message = new Message({message: decoded.message, userId: decoded.userId});
                    await message.save();
                } catch {
                    ws.send(JSON.stringify({type: 'ERROR', error: 'Internal Server Error'}));
                }
                const messages = await Message.find().populate('userId', 'username');
                const lastMsg = messages[messages.length - 1];

                const messageData = {
                    type: 'PRIVATE_MESSAGE',
                    _id: lastMsg._id,
                    message: lastMsg.message,
                    userId: lastMsg.userId,
                    datetime: lastMsg.datetime,
                    from: decoded.user.username,
                    to: specificUser.username,
                };

                sendMessageToSpecificUser(specificUser.token, JSON.stringify(messageData));

                Object.keys(obj).forEach(key => {
                    const userConnection = obj[key];
                    userConnection.send(JSON.stringify(messageData));
                });
                break;

            case 'CLOSE_CONNECTION':
                const userToken = Object.keys(activeConnections).find(userToken => userToken === decoded.user.token);

                if (userToken) {
                    delete activeConnections[userToken];
                    await getActiveUsersAndMessages('DISCONNECTED');
                } else {
                    ws.send(JSON.stringify({type: 'ERROR', error: 'Internal server error'}));
                }
                break;
            default:
                console.log('Unknown type');
        }
    });

    ws.on('close', async () => {
        delete activeConnections[token];

        await getActiveUsersAndMessages('DISCONNECTED');
    });
});

const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(() => {
        console.log('Exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));
