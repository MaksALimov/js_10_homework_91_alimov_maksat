const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const User = require('./models/User');
const Message = require('./models/Message');
const config = require('./config');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [firstUser, secondUser] = await User.create({
        username: 'jack',
        password: '123',
        token: nanoid(),
        role: 'user',
    }, {
        username: 'moderator',
        password: '1234',
        token: nanoid(),
        role: 'moderator',
    });

    await Message.create({
        message: 'First user\s message',
        userId: firstUser,
        datetime: new Date("12/03/2021 21:41"),
    }, {
        message: 'Moderator\s message',
        userId: secondUser,
        datetime: new Date("12/03/2021 21:43"),
    })

    await mongoose.connection.close();
};

run().catch(e => console.error(e));