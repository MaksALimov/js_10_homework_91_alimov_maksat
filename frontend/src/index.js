import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import store from "./store/configureStore";
import history from "./history";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const app = (
    <Provider store={store}>
        <Router history={history}>
            <ToastContainer/>
            <App/>
        </Router>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
