import React from 'react';
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {useSelector} from "react-redux";

const useStyles = makeStyles(() => ({
    onlineUserContainer: {
        flex: '1',
        boxShadow: 'rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px',
        textAlign: 'center',
        marginBottom: '40px',
    },

    title: {
        textAlign: 'center',
        margin: '20px 0',
        fontWeight: 'bold',
        fontSize: '28px',
    },


    onlineUsers: {
        boxShadow: 'rgba(0, 0, 0, 0.25) 0px 0.0625em 0.0625em, rgba(0, 0, 0, 0.25) 0px 0.125em 0.5em, rgba(255, 255, 255, 0.1) 0px 0px 0px 1px inset',
        padding: '20px',
        borderRadius: '15px',
        marginBottom: '20px',
    },

    onlineUsersName: {
        fontSize: '20px',
        margin: '10px',
    },
}));

const OnlineUsers = () => {
    const classes = useStyles();
    const activeUsers = useSelector(state => state.general.users);

    const filtered = activeUsers.filter(user => user !== null);

    return (
        <Grid item xs={4} className={classes.onlineUserContainer}>
            <Typography component="h4" className={classes.title}>
                Online Users
            </Typography>
            {filtered.map(activeUser => (
                <Grid item key={activeUser._id} className={classes.onlineUsers}>
                    <Typography className={classes.onlineUsersName}>
                        {activeUser.username}
                    </Typography>
                </Grid>
            ))}
        </Grid>
    );
};

export default OnlineUsers;