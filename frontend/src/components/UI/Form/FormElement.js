import React from 'react';
import PropTypes from 'prop-types';
import {Grid, makeStyles, TextField} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    textField: {
        margin: '10px 0',
        fontSize: '20px',
    },
}));

const FormElement = ({label, name, value, onChange, required, error, autoComplete, type}) => {
    const classes = useStyles();

    return (
        <Grid item>
            <TextField
                type={type}
                required={required}
                autoComplete={autoComplete}
                label={label}
                fullWidth
                variant="outlined"
                name={name}
                className={classes.textField}
                inputProps={{className: classes.textField}}
                value={value}
                onChange={onChange}
                error={Boolean(error)}
                helperText={error}
            />
        </Grid>
    );
};

FormElement.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
    type: PropTypes.string,
    helperText: PropTypes.string,
};

export default FormElement;