import React from 'react';
import {AppBar, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import Anonymous from "../Anonymous/Anonymous";
import {useSelector} from "react-redux";
import User from "../User/User";

const useStyles = makeStyles(() => ({
    appBar: {
        padding: '10px',
    },

    mainLink: {
        color: '#fff',
        textDecoration: 'none',
    },

    title: {
        fontSize: '35px',
        fontWeight: 'bold',
        '&:hover': {
            cursor: 'pointer',
        },
    },
}));

const Header = () => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);
    return (
        <>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Typography variant="h6" className={classes.title}>
                            <Link to="/" className={classes.mainLink}>
                                Chat
                            </Link>
                        </Typography>
                        {user ? <User/> : <Anonymous/>}
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </>
    );
};

export default Header;