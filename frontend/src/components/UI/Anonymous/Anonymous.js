import React from 'react';
import {Button, Grid, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles(() => ({
    loginBtn: {
        fontSize: '24px',
        marginLeft: '20px',
    },

    login: {
        color: '#fff',
        textDecoration: 'none',
    },

    signUp: {
        fontSize: '24px',
        color: '#fff',
        textDecoration: 'none',
    },
}));

const Anonymous = () => {
    const classes = useStyles();

    return (
        <Grid item>
            <Button>
                <Link to="/register" className={classes.signUp}>
                    Sign up
                </Link>
            </Button>
            <Button className={classes.loginBtn}>
                <Link to="/login" className={classes.login}>
                    Login
                </Link>
            </Button>
        </Grid>
    );
};

export default Anonymous;