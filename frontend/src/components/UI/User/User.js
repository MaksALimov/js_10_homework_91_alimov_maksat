import React from 'react';
import {Button, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";

const useStyles = makeStyles(() => ({
    username: {
        fontSize: '30px',
        marginRight: '40px',
    },

    logoutBtn: {
        fontSize: '18px',
        fontWeight: 'bold',
    },
}));

const User = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <Grid item>
            <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography className={classes.username}>Welcome {user.username} !</Typography>
                </Grid>
                <Grid item>
                    <Button
                        variant="contained"
                        className={classes.logoutBtn}
                        onClick={() => dispatch(logoutUser())}
                    >
                        Logout
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default User;