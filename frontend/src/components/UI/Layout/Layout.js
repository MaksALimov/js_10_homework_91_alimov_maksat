import React from 'react';
import {Container, CssBaseline, makeStyles} from "@material-ui/core";
import Header from "../Header/Header";

const useStyles = makeStyles(() => ({
    main: {
        margin: '50px 0',
    },
}));

const Layout = ({children}) => {
    const classes = useStyles();
    return (
        <>
            <CssBaseline/>
            <Header/>
            <main className={classes.main}>
                <Container>
                    {children}
                </Container>
            </main>
        </>
    );
};

export default Layout;