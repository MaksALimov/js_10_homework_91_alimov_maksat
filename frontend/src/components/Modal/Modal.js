import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import Backdrop from "../UI/Backdrop/Backdrop";
import {useDispatch, useSelector} from "react-redux";
import {setModal} from "../../store/actions/usersActions";

const useStyles = makeStyles(() => ({
    modal: {
        position: 'fixed',
        zIndex: '500',
        backgroundColor: 'white',
        width: '70%',
        border: '1px solid #ccc',
        boxShadow: '1px 1px 1px black',
        padding: '16px',
        left: '15%',
        top: '30%',
        boxSizing: 'border-box',
        transition: 'all 0.3s ease-out',
    },

    buttons: {
        margin: '10px 0',
    },
}));

const Modal = ({ws}) => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);
    const [message, setMessage] = useState('');
    const userId = useSelector(state => state.users.userId);
    const userName = useSelector(state => state.users.userName);
    const dispatch = useDispatch();

    const sendPrivateMessage = () => {
        ws.send(JSON.stringify({type: 'PRIVATE_MESSAGE', userId, message, user}))
        setMessage('');
        dispatch(setModal(false));
    };

    return (
        <>
            <Backdrop/>
            <Grid container direction="column" className={classes.modal} alignItems="center">
                <Grid item>
                    <Typography component="h3">Send private message to {userName}</Typography>
                </Grid>
                <Grid item component="form">
                    <TextField
                        variant="outlined"
                        value={message}
                        onChange={e => setMessage(e.target.value)}
                    >
                    </TextField>
                </Grid>
                <Button
                    variant="contained"
                    onClick={() => dispatch(setModal(false))}
                    className={classes.buttons}
                >
                    Close
                </Button>
                <Button
                    variant="contained"
                    onClick={sendPrivateMessage}
                    className={classes.buttons}
                >
                    Send
                </Button>
            </Grid>
        </>
    );
};

export default Modal;