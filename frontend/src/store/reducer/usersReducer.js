import {
    CLEAR_FORM_INPUTS_ERRORS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS, SET_MODAL
} from "../actions/usersActions";

const initialState = {
    user: null,
    registerError: null,
    registerLoading: null,
    loginError: null,
    loginLoading: null,
    modal: false,
    userId: null,
    userName: null,
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_USER_REQUEST:
            return {...state, registerError: null, registerLoading: true, user: null};

        case REGISTER_USER_SUCCESS:
            return {...state, user: action.payload, registerLoading: false, registerError: false};

        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.payload, registerLoading: false};

        case LOGIN_USER_REQUEST:
            return {...state, loginError: null, loginLoading: true};

        case LOGIN_USER_SUCCESS:
            return {...state, user: action.payload, loginLoading: false, loginError: null};

        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.payload, loginLoading: false};

        case LOGOUT_USER:
            return {...state, user: null};

        case CLEAR_FORM_INPUTS_ERRORS:
            return {...state, registerError: null, loginError: null};

        case SET_MODAL:
            return {...state, modal: action.payload, userId: action.userId, userName: action.userName};

        default:
            return state;
    }
};

export default userReducer;