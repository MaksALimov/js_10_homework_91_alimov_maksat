const initialState = {
    users: [],
    messages: [],
    error: null,
};

const generalReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CONNECTED':
            return {...state, users: action.users, messages: action.messages};

        case 'NEW_MESSAGE':
            return {...state, users: action.users, messages: [...state.messages, action.messages].slice(-30)};

        case 'DELETED_MESSAGE':
            return {...state, messages: state.messages.filter(message => message._id !== action.messages._id)};

        case 'PRIVATE_MESSAGE':
            return {
                ...state,
                messages: [...state.messages,
                    {
                        message: action.message,
                        userId: action.userId,
                        datetime: action.datetime,
                        _id: action._id,
                        from: action.from,
                        to: action.to
                    }
                ],
            };

        case 'DISCONNECTED':
            return {...state, users: action.users};

        case 'ERROR':
            return {...state, error: action.error};

        default:
            return state;
    }
};

export default generalReducer;