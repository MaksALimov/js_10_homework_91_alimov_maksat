import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import userReducer from "./reducer/usersReducer";
import generalReducer from "./reducer/generalReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";

const rootReducer = combineReducers({
    users: userReducer,
    general: generalReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(
   applyMiddleware(thunk)
));

store.subscribe(() => {
    saveToLocalStorage({
        users: store.getState().users,
    });
});


export default store;
