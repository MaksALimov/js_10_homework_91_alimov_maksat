import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Chat from "./containers/Chat/Chat";
import {useSelector} from "react-redux";
import {Redirect} from 'react-router-dom';

const App = () => {
    const user = useSelector(state => state.users.user);

    const ChatRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route{...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <ChatRoute
                    path="/"
                    exact
                    component={Chat}
                    isAllowed={Boolean(user)}
                    redirectTo="/register"
                />
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;
