import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    title: {
        textAlign: 'center',
        margin: '20px 0',
        fontWeight: 'bold',
        fontSize: '28px',
    },

    sendBtnContainer: {
        textAlign: 'center',
    },

    sendBtn: {
        fontSize: '18px',
        padding: '10px 30px',
        textTransform: 'capitalize',
    },

    username: {
        fontSize: '20px',
        fontWeight: 'bold',
        margin: '10px 15px 10px 20px',
        '&:hover': {
            cursor: 'pointer',
        },
    },

    message: {
        margin: '10px 20px',
        fontSize: '17px',
    },

    chatRoomContainer: {
        boxShadow: 'rgba(14, 30, 37, 0.12) 0px 2px 4px 0px, rgba(14, 30, 37, 0.32) 0px 2px 16px 0px',
        marginBottom: '40px',
    },

    userContainer: {
        textAlign: 'center',
        width: '40%',
        boxShadow: 'rgba(0, 0, 0, 0.25) 0px 0.0625em 0.0625em, rgba(0, 0, 0, 0.25) 0px 0.125em 0.5em, rgba(255, 255, 255, 0.1) 0px 0px 0px 1px inset',
        margin: '20px 0',
        borderRadius: '15px',
    },

    deleteBtn: {
        margin: '5px 0 20px',
    },

    errors: {
        textAlign: 'center',
        fontSize: '45px',
        fontWeight: 'bold',
    },
}));

const ChatStyles = () => (
    useStyles()
);

export default ChatStyles;