import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import OnlineUsers from "../../components/OnlineUsers/OnlineUsers";
import dayjs from "dayjs";
import Modal from "../../components/Modal/Modal";
import {setModal} from "../../store/actions/usersActions";
import chatStyles from "./ChatStyles";
import {Button, Grid, TextField, Typography,} from "@material-ui/core";

const Chat = () => {
    const classes = chatStyles();
    const ws = useRef(null);
    const [message, setMessage] = useState('');
    const messages = useSelector(state => state.general.messages);
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const modal = useSelector(state => state.users.modal);
    const error = useSelector(state => state.general.error);

    useEffect(() => {
        ws.current = new WebSocket(`ws://localhost:8000/chat?token=${user.token}`);

        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);
            dispatch(decoded);
        };

        ws.current.onclose = () => {
            const interval = setInterval(() => {
                ws.current = new WebSocket(`ws://localhost:8000/chat?token=${user.token}`);

                ws.current.onopen = () => {
                    ws.current.onmessage = event => {
                        const decoded = JSON.parse(event.data);
                        dispatch(decoded);
                    };
                    clearInterval(interval);
                };
            }, 2000);

        };

        return () => {
            ws.current.send(JSON.stringify({type: 'CLOSE_CONNECTION', user}));
        }
    }, [user, dispatch]);

    const sendMessage = e => {
        e.preventDefault();
        ws.current.send(JSON.stringify({type: 'CREATE_MESSAGE', message}));
        setMessage('');
    };

    const deleteMessage = messageId => {
        ws.current.send(JSON.stringify({type: 'DELETE_MESSAGE', messageId}));
    };

    return (
        <>
            {error ? (
                <Typography className={classes.errors}>{error}</Typography>
            ) : (
                <Grid container justifyContent="space-between">
                    <OnlineUsers/>
                    <Grid item xs={7} className={classes.chatRoomContainer}>
                        <Typography component="h4" className={classes.title}>
                            Chat Room
                        </Typography>
                        {messages.map(message => (
                            <Grid
                                key={message._id}
                                container
                                justifyContent="space-evenly"
                                alignItems="center">
                                <Grid item className={classes.userContainer}>
                                    <Typography
                                        className={classes.username}
                                        onClick={() => dispatch(setModal(true, message.userId._id, message.userId.username))}
                                    >
                                        {message.username || message.userId.username}
                                    </Typography>
                                    <Typography
                                        className={classes.message}>
                                        {message.message}
                                    </Typography>
                                    {user.role === 'moderator' ? (
                                        <Button
                                            variant="contained"
                                            className={classes.deleteBtn}
                                            onClick={() => deleteMessage(message._id)}
                                        >
                                            Delete
                                        </Button>
                                    ) : null}
                                </Grid>
                                <Grid item>
                                    <Typography>
                                        {dayjs(message?.datetime).format('YYYY-MM-DD HH:mm')}
                                    </Typography>
                                </Grid>
                            </Grid>
                        ))}
                    </Grid>
                    <Grid container
                          component="form"
                          justifyContent="flex-end"
                          alignItems="center"
                          onSubmit={sendMessage}
                    >
                        <Grid item xs={5}>
                            <TextField
                                label="Enter message"
                                onChange={e => setMessage(e.target.value)}
                                value={message}
                                name="message"
                                fullWidth
                                variant="outlined"
                                multiline
                                required
                            />
                        </Grid>
                        <Grid item xs={2} className={classes.sendBtnContainer}>
                            <Button type="submit" variant="contained" className={classes.sendBtn}>
                                Send
                            </Button>
                        </Grid>
                    </Grid>
                    {modal ? <Modal ws={ws.current}/> : null}
                </Grid>
            )}
        </>
    );
};

export default Chat;