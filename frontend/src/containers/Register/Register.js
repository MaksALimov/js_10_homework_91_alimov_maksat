import React, {useEffect, useState} from 'react';
import {Container, Grid, makeStyles, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {clearFormInputsErrors, registerUser} from "../../store/actions/usersActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(() => ({
    signUp: {
        textAlign: 'center',
        fontSize: '20px',
        color: '#fff',
        fontWeight: 'bold',
        margin: '25px 0',
    },

    signIn: {
        fontWeight: 'bold',
        fontSize: '20px',
        margin: '20px 0',
        textDecoration: 'none',
    },

    registrationTitle: {
        textAlign: 'center',
        margin: '15px 0',
        fontSize: '30px',
    },

    networkError: {
        textAlign: 'center',
        fontSize: '40px',
        fontWeight: 'bold',
    },

    container: {
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
        marginTop: '140px',
        padding: '50px 20px',
    },
}));

const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    useEffect(() => {
        return () => {
            dispatch(clearFormInputsErrors());
        };
    }, [dispatch]);

    const [user, setUser] = useState({
        username: '',
        password: '',
    });

    const error = useSelector(state => state.users.registerError);
    const registerLoading = useSelector(state => state.users.registerLoading);

    const onInputChange = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const onSubmitForm = e => {
        e.preventDefault();
        dispatch(registerUser({...user}));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (error) {
            return undefined;
        }
    };

    return (
        <Container maxWidth="xs" className={classes.container}>
            <Grid container direction="column" component="form" onSubmit={onSubmitForm}>
                {typeof error === 'string' ? (
                    <Typography className={classes.networkError} component="h2">{error}</Typography>
                ) : (
                    <Grid item>
                        <Typography
                            component="h2"
                            variant="h6"
                            className={classes.registrationTitle}
                        >
                            Sign up
                        </Typography>
                    </Grid>
                )}
                <FormElement
                    type="text"
                    autoComplete="new-username"
                    label="Username"
                    name="username"
                    value={user.username}
                    onChange={onInputChange}
                    error={getFieldError('username')}
                >
                </FormElement>
                <FormElement
                    type="password"
                    autoComplete="new-password"
                    label="Password"
                    name="password"
                    value={user.password}
                    onChange={onInputChange}
                    error={getFieldError('password')}
                >
                </FormElement>
                <Grid item>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.signUp}
                        loading={registerLoading}
                        disabled={registerLoading}
                    >
                        Sign up
                    </ButtonWithProgress>
                </Grid>
                <Grid item container justifyContent="flex-end">
                    <Link variant="body2" to="/login" className={classes.signIn}>
                        Already have an account ? Sign in
                    </Link>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Register;