import React, {useEffect, useState} from 'react';
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import Typography from "@material-ui/core/Typography";
import FormElement from "../../components/UI/Form/FormElement";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {Avatar, Container, Grid, makeStyles} from "@material-ui/core";
import {clearFormInputsErrors, loginUser} from "../../store/actions/usersActions";
import {Alert} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyle = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },

    form: {
        marginTop: theme.spacing(1),
    },

    submit: {
        margin: theme.spacing(3, 0, 2),
    },

    alert: {
        marginTop: theme.spacing(3),
        width: '100%',
    },

    container: {
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
        marginTop: '140px',
        padding: '50px 20px',
    },

    signUp: {
        fontWeight: 'bold',
        fontSize: '20px',
        margin: '20px 0',
        textDecoration: 'none',
    },
}));


const Login = () => {
    const classes = useStyle();
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.loginError);
    const loading = useSelector(state => state.users.loginLoading);
    const [user, setUser] = useState({
        username: '',
        password: '',
    });

    useEffect(() => {
        return () => {
            dispatch(clearFormInputsErrors());
        };
    }, [dispatch]);

    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...user}));
    };

    return (
        <Container component="section" maxWidth="xs" className={classes.container}>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h6">
                    Sign in
                </Typography>
                {
                    error &&
                    <Alert severity="error" className={classes.alert}>
                        {error.message || error}
                    </Alert>
                }
                <Grid
                    component="form"
                    container
                    direction="column"
                    className={classes.form}
                    onSubmit={submitFormHandler}
                    spacing={2}
                >
                    <FormElement
                        type="text"
                        autoComplete="current-username"
                        label="Username"
                        name="username"
                        value={user.username}
                        onChange={inputChangeHandler}
                    />

                    <FormElement
                        type="password"
                        autoComplete="current-password"
                        label="Password"
                        name="password"
                        value={user.password}
                        onChange={inputChangeHandler}
                    />
                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            loading={loading}
                            disabled={loading}
                        >
                            Sign in
                        </ButtonWithProgress>
                    </Grid>

                    <Grid item container justifyContent="flex-end">
                        <Link variant="body2" to="/register" className={classes.signUp}>
                            Or sign up
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Login;